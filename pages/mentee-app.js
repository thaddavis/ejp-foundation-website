import Navbar from '../components/Navbar';

import React, { useEffect, useState } from 'react';
import { loadReCaptcha, ReCaptcha } from 'react-recaptcha-v3'
import { useForm } from "react-hook-form";

import axios from 'axios'

import Footer from '../components/Footer'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function MenteeAppForm() {

    const [recaptcha, setRecaptcha] = useState('')
    const [apiCallState, setApiCallState] = useState({})

    let verifyCallback = (recaptchaToken) => { setRecaptcha(recaptchaToken) }

    useEffect(() => {
        loadReCaptcha('6LfkO9EaAAAAAD2q8XFV5GmdnBPrq0QkIiIglC1r', (data) => {
            console.log('reCAPTCHA callback')
        });
    }, [])


    const { getValues, register, handleSubmit, watch, formState: { errors } } = useForm(
        {
            // defaultValues: {
            //     name: "Jean-Pierre Matisse",
            //     phone: "305-336-6417",
            //     email: "bluebill1049@hotmail.com",
            //     contact_method: "other",
            //     specify_other_contact_method: "Instagram",
            //     interest_and_hobbies: "Music, R&B, Acting",
            //     why_participate: "Looking to connect and network",
            //     describe_school_performance: "B's and C's",
            //     has_friends: "A few friends yes",
            //     child_having_problems: "I am feeling in need of inspiration",
            //     additional_background_info: "I am gay",
            //     any_religious_preferences: "Zoroastrianism"
            // }
        }
    );

    const onSubmit = data => {
        setApiCallState({
            ...apiCallState,
            loading: true,
            data: null,
            error: null
        })

        axios.post('/api/mentee-app-form',
            {
                ...data,
                recaptcha
            })
            .then(function (response) {

                setApiCallState({
                    ...apiCallState,
                    loading: false,
                    data: response.data,
                    error: null
                })

            })
            .catch(function (error) {
                setApiCallState({
                    ...apiCallState,
                    loading: false,
                    data: null,
                    error: (
                        error &&
                        error.response &&
                        error.response.data &&
                        error.response.data.message
                    ) || error.toString()
                })

                toast.error(
                    (
                        error &&
                        error.response &&
                        error.response.data &&
                        error.response.data.message
                    ) || error.toString(), {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined
                }
                )

            })

    }

    watch("contact_method")

    let contact_method = getValues('contact_method')

    return (
        <>
            <Navbar />
            <header style={{ textAlign: 'center' }}>
                <h2>Mentee App</h2>
            </header>

            {
                apiCallState &&
                    apiCallState.data
                    ?
                    <main style={{
                        textAlign: 'center'
                        
                    }}>
                        <h1 style={{
                            color: '#006916'
                        }}>
                            Successfully submitted mentee application!
                        </h1>
                    </main>
                :
                    <form>

                        <div>
                            <label className="desc" htmlFor="name">Full Name</label>
                            <div>
                                <input
                                    id="name"
                                    name="name"
                                    type="text"
                                    placeholder="Name"
                                    className="field text fn"
                                    {...register("name", {
                                        required: true
                                    })}
                                />
                                <br />
                                {errors.name ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>

                        </div>

                        <div>
                            <label className="desc" htmlFor="email">Email</label>
                            <div>
                                <input
                                    id="email"
                                    name="email"
                                    type="email"
                                    placeholder="Email"
                                    maxLength="255"
                                    {...register("email", {
                                        required: true
                                    })}
                                />
                                <br />
                                {errors.email ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        <div>
                            <label className="desc" htmlFor="phone">
                                Phone
                    </label>
                            <div>
                                <input
                                    id="phone"
                                    name="phone"
                                    type="text"
                                    placeholder="Phone Number"
                                    {...register("phone", {
                                        required: true
                                    })}
                                />
                                <br />
                                {errors.phone ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        <div>
                            <label className="desc" htmlFor="contact_method">
                                Select Contact Method
                    </label>
                            <div>
                                <select
                                    id="contact_method"
                                    name="contact_method"
                                    className="field select medium"
                                    {...register("contact_method", {
                                        required: true
                                    })}
                                >
                                    <option value="">Select Contact Method</option>
                                    <option value="phone">Phone</option>
                                    <option value="email">Email</option>
                                    <option value="other">Other</option>
                                </select>
                                <br />
                                {errors.contact_method ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        {
                            contact_method === 'other' &&
                            <div>
                                <label className="desc" htmlFor="specify_other_contact_method">If other, please specify preferred contact</label>
                                <div>
                                    <input
                                        id="specify_other_contact_method"
                                        name="specify_other_contact_method"
                                        type="text"
                                        placeholder="Specify Contact Method"
                                        maxLength="255"
                                        {...register("specify_other_contact_method", {})}
                                    />
                                    <br />
                                    {errors.specify_other_contact_method ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                                </div>
                            </div>
                        }

                        <div>
                            <label className="desc" htmlFor="interest_and_hobbies">
                                Please list your interests, hobbies, and any other comments you wish to include.
                    </label>

                            <div>
                                <textarea
                                    id="interest_and_hobbies"
                                    name="interest_and_hobbies"
                                    spellCheck="true"
                                    rows="10"
                                    cols="50"
                                    placeholder="List you/your child's interests and hobbies"
                                    {...register("interest_and_hobbies", {
                                        required: true
                                    })}
                                >
                                </textarea>
                                <br />
                                {errors.interest_and_hobbies ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        <div>
                            <label className="desc" htmlFor="why_participate">
                                Why do you/your child want to participate in a mentoring program?
                    </label>

                            <div>
                                <textarea
                                    id="why_participate"
                                    name="why_participate"
                                    spellCheck="true"
                                    rows="10"
                                    cols="50"
                                    placeholder="Why do you/your child want to participate in a mentoring program?"
                                    {...register("why_participate", {
                                        required: true
                                    })}
                                >
                                </textarea>
                                <br />
                                {errors.why_participate ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        <div>
                            <label className="desc" htmlFor="describe_school_performance">
                                Describe your child’s school performance including grades, homework, attendance and behavior, etc.
                    </label>

                            <div>
                                <textarea
                                    id="describe_school_performance"
                                    name="describe_school_performance"
                                    spellCheck="true"
                                    rows="10"
                                    cols="50"
                                    placeholder="Describe your child’s school performance including grades, homework, attendance and behavior, etc."
                                    {...register("describe_school_performance", {
                                        required: true
                                    })}
                                >
                                </textarea>
                                <br />
                                {errors.describe_school_performance ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        <div>
                            <label className="desc" htmlFor="has_friends">
                                Does your child have friends?
                    </label>

                            <div>
                                <textarea
                                    id="has_friends"
                                    name="has_friends"
                                    spellCheck="true"
                                    rows="10"
                                    cols="50"
                                    placeholder="Does your child have friends?"
                                    {...register("has_friends", {
                                        required: true
                                    })}
                                >
                                </textarea>
                                <br />
                                {errors.has_friends ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        <div>
                            <label className="desc" htmlFor="child_having_problems">
                                Is your child currently having problems either at home or at school? If so, provide details.
                    </label>

                            <div>
                                <textarea
                                    id="child_having_problems"
                                    name="child_having_problems"
                                    spellCheck="true"
                                    rows="10"
                                    cols="50"
                                    placeholder="Is your child currently having problems either at home or at school?"
                                    {...register("child_having_problems", {
                                        required: true
                                    })}
                                >
                                </textarea>
                                <br />
                                {errors.child_having_problems ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        <div>
                            <label className="desc" htmlFor="additional_background_info">
                                Can you provide any additional background information that may be helpful in matching your child with an appropriate mentor?
                    </label>

                            <div>
                                <textarea
                                    id="additional_background_info"
                                    name="additional_background_info"
                                    spellCheck="true"
                                    rows="10"
                                    cols="50"
                                    placeholder="Can you provide any additional background information that may be helpful in matching your child with an appropriate mentor?"
                                    {...register("additional_background_info", {
                                        required: true
                                    })}
                                >
                                </textarea>
                                <br />
                                {errors.additional_background_info ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        <div>
                            <label className="desc" htmlFor="any_religious_preferences">
                                Do you have any religious preferences you would like for us to take into consideration?
                    </label>

                            <div>
                                <textarea
                                    id="any_religious_preferences"
                                    name="any_religious_preferences"
                                    spellCheck="true"
                                    rows="10"
                                    cols="50"
                                    placeholder="Do you have any religious preferences you would like for us to take into consideration?"
                                    {...register("any_religious_preferences", {
                                        required: true
                                    })}
                                >
                                </textarea>
                                <br />
                                {errors.any_religious_preferences ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                            </div>
                        </div>

                        <div>
                            <div>
                                <input
                                    id="saveForm"
                                    name="saveForm"
                                    type="submit"
                                    value="Submit"
                                    onClick={handleSubmit(onSubmit)}
                                />
                            </div>
                        </div>

                        <ReCaptcha
                            sitekey="6LfkO9EaAAAAAD2q8XFV5GmdnBPrq0QkIiIglC1r"
                            action='submit'
                            verifyCallback={verifyCallback}
                        />

                    </form>
            }

            <Footer/>

            <ToastContainer />
        </>
    );
}