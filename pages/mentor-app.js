import Navbar from '../components/Navbar';

import useSWR from 'swr'

import React, { useEffect } from 'react';
import { loadReCaptcha, ReCaptcha } from 'react-recaptcha-v3'
import { useForm } from "react-hook-form";

import Footer from '../components/Footer'
import axios from 'axios'

// import { Select } from "./FormComponents/Select";

export default function MentorAppForm() {

    let recaptcha
    // let verifyCallback = (recaptchaToken) => {
    //     recaptcha = recaptchaToken
    //     console.log(recaptchaToken, "<= your recaptcha token")
    // }

    // useEffect(() => {
    //     loadReCaptcha('6LfkO9EaAAAAAD2q8XFV5GmdnBPrq0QkIiIglC1r', (data) => {
    //         console.log('callback')
    //     });
    // }, [])


    const { register, handleSubmit, watch, formState: { errors } } = useForm(
        {
            defaultValues: {
                name: "bill",
                phone: "luo",
                email: "bluebill1049@hotmail.com",
                contact_method: "other",
                specify_other_contact_method: "IG",
                interest_and_hobbies: "a",
                why_participate: "a",
                describe_school_performance: "a",
                has_friends: "a",
                child_having_problems: "a",
                additional_background_info: "a",
                any_religious_preferences: "a"
            }
        }
    );
    const onSubmit = data => {
        console.log('___ SUBMIT ___');
        console.log('data', data);
        console.log('errors', errors);

        // axios.post('https://www.google.com/recaptcha/api/siteverify', {
        //     secret: '6LfkO9EaAAAAANRdqsO_UKO24c19yMa9vpkc50yz',
        //     response: recaptcha
        // })
        //     .then(function (response) {
        //         // handle success
        //         console.log(response);
        //     })
        //     .catch(function (error) {
        //         // handle error
        //         console.log(error);
        //     })
        //     .then(function () {
        //         // always executed
        //     });

        // axios.post('/api/hello', {
        //     message: 'test'
        // })
        //     .then(function (response) {
        //         // handle success
        //         console.log(response);
        //     })
        //     .catch(function (error) {
        //         // handle error
        //         console.log(error);
        //     })
        //     .finally(function () {
        //         // always executed
        //         console.log('always executed')
        //     });
    }

    watch("name")
    watch("email")

    // console.log('render errors', errors)

    return (
        <>
            <Navbar />
            <header style={{ textAlign: 'center' }}>
                <h2>Mentor App</h2>
            </header>

            <main style={{
                textAlign: 'center',
                padding: '4em'
            }}>
                Mentor Application Coming Soon!
            </main>

            {/* <form>

                <div>
                    <label className="desc" htmlFor="name">Full Name</label>
                    <div>
                        <input
                            id="name"
                            name="name"
                            type="text"
                            className="field text fn"
                            tabIndex="1"
                            {...register("name", {
                                required: true
                            })}
                        />
                        <br />
                        {errors.name ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>

                </div>

                <div>
                    <label className="desc" htmlFor="email">Email</label>
                    <div>
                        <input
                            id="email"
                            name="email"
                            type="email"
                            maxLength="255"
                            tabIndex="2"
                            {...register("email", {
                                required: true
                            })}
                        />
                        <br />
                        {errors.email ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <label className="desc" htmlFor="phone">
                        Phone
                    </label>
                    <div>
                        <input
                            id="phone"
                            name="phone"
                            type="text"
                            tabIndex="3"
                            {...register("phone", {
                                required: true
                            })}
                        />
                        <br />
                        {errors.phone ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <label className="desc" htmlFor="contact_method">
                        Select Contact Method
                    </label>
                    <div>
                        <select 
                            id="contact_method" 
                            name="contact_method" 
                            className="field select medium" 
                            tabIndex="4"
                            {...register("contact_method", {
                                required: true
                            })}
                        >
                            <option value="phone">Phone</option>
                            <option value="email">Email</option>
                            <option value="other">Other</option>
                        </select>
                        <br/>
                        {errors.contact_method ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <label className="desc" htmlFor="interest_and_hobbies">
                        Please list your interests, hobbies, and any other comments you wish to include.
                    </label>

                    <div>
                        <textarea
                            id="interest_and_hobbies"
                            name="interest_and_hobbies"
                            spellCheck="true"
                            rows="10"
                            cols="50"
                            tabIndex="5"
                            {...register("interest_and_hobbies", {
                                required: true
                            })}
                        >
                        </textarea>
                        <br />
                        {errors.interest_and_hobbies ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <label className="desc" htmlFor="why_participate">
                        Why do you/your child want to participate in a mentoring program?
                    </label>

                    <div>
                        <textarea
                            id="why_participate"
                            name="why_participate"
                            spellCheck="true"
                            rows="10"
                            cols="50"
                            tabIndex="6"
                            {...register("why_participate", {
                                required: true
                            })}
                        >
                        </textarea>
                        <br />
                        {errors.why_participate ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <label className="desc" htmlFor="describe_school_performance">
                        Describe your child’s school performance including grades, homework, attendance and behavior, etc.
                    </label>

                    <div>
                        <textarea
                            id="describe_school_performance"
                            name="describe_school_performance"
                            spellCheck="true"
                            rows="10"
                            cols="50"
                            tabIndex="7"
                            {...register("describe_school_performance", {
                                required: true
                            })}
                        >
                        </textarea>
                        <br />
                        {errors.describe_school_performance ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <label className="desc" htmlFor="has_friends">
                        Does your child have friends?
                    </label>

                    <div>
                        <textarea
                            id="has_friends"
                            name="has_friends"
                            spellCheck="true"
                            rows="10"
                            cols="50"
                            tabIndex="8"
                            {...register("has_friends", {
                                required: true
                            })}
                        >
                        </textarea>
                        <br />
                        {errors.has_friends ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <label className="desc" htmlFor="child_having_problems">
                        Is your child currently having problems either at home or at school? If so, provide details.
                    </label>

                    <div>
                        <textarea
                            id="child_having_problems"
                            name="child_having_problems"
                            spellCheck="true"
                            rows="10"
                            cols="50"
                            tabIndex="9"
                            {...register("child_having_problems", {
                                required: true
                            })}
                        >
                        </textarea>
                        <br />
                        {errors.child_having_problems ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <label className="desc" htmlFor="additional_background_info">
                        Can you provide any additional background information that may be helpful in matching your child with an appropriate mentor?
                    </label>

                    <div>
                        <textarea
                            id="additional_background_info"
                            name="additional_background_info"
                            spellCheck="true"
                            rows="10"
                            cols="50"
                            tabIndex="10"
                            {...register("additional_background_info", {
                                required: true
                            })}
                        >
                        </textarea>
                        <br />
                        {errors.additional_background_info ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <label className="desc" htmlFor="any_religious_preferences">
                        Do you have any religious preferences you would like for us to take into consideration?
                    </label>

                    <div>
                        <textarea
                            id="any_religious_preferences"
                            name="any_religious_preferences"
                            spellCheck="true"
                            rows="10"
                            cols="50"
                            tabIndex="11"
                            {...register("any_religious_preferences", {
                                required: true
                            })}
                        >
                        </textarea>
                        <br />
                        {errors.any_religious_preferences ? <span style={{ color: 'red' }}>required</span> : <span>_</span>}
                    </div>
                </div>

                <div>
                    <div>
                        <input
                            id="saveForm"
                            name="saveForm"
                            type="submit"
                            value="Submit"
                            onClick={handleSubmit(onSubmit)}
                        />
                    </div>
                </div>

                <ReCaptcha
                    ref={ref => recaptcha = ref}
                    sitekey="6LfkO9EaAAAAAD2q8XFV5GmdnBPrq0QkIiIglC1r"
                    action='submit'
                    verifyCallback={verifyCallback}
                />

            </form> */}

            <Footer/>
        </>
    );
}