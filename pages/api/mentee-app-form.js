import axios from 'axios'
import { SESClient, SendTemplatedEmailCommand } from "@aws-sdk/client-ses"

const REGION = "us-east-1" //e.g. "us-east-1"
// Create SNS service object.
const sesClient = new SESClient({ region: REGION })

export default async (req, res) => {

  const {
    method,
    body
  } = req

  const {
    name,
    phone,
    email,
    contact_method,
    specify_other_contact_method,
    interest_and_hobbies,
    why_participate,
    describe_school_performance,
    has_friends,
    child_having_problems,
    additional_background_info,
    any_religious_preferences,
    recaptcha // added recaptcha
  } = body

  let preferredContactMethod = ``
  if (contact_method === 'other') preferredContactMethod = specify_other_contact_method
  else preferredContactMethod = contact_method

  switch (method) {
    case 'POST':
      // Validate Captcha
      const recaptchaParams = new URLSearchParams();
      recaptchaParams.append('secret', '6LfkO9EaAAAAANRdqsO_UKO24c19yMa9vpkc50yz');
      recaptchaParams.append('response', recaptcha);

      try {
        let captchaResponse = await axios.post('https://www.google.com/recaptcha/api/siteverify', recaptchaParams)
        
        if (
          (captchaResponse && captchaResponse.data && !captchaResponse.data.success) ||
          (captchaResponse && captchaResponse.data && captchaResponse.data.score < 0.7)
        ) {
          throw new Error('You did not pass the reCAPTCHA. Please reload form and try again.')
        }

      } catch(e) {
        res.status(500).json({ message: e.toString() })
        return
      }
      
      try {
        // Then send email
        let params = {
          Destination: {
            CcAddresses: [],
            ToAddresses: [
              // 'bounce@simulator.amazonses.com',
              'thadduval.lavud@gmail.com',
              'faisalhuda37@yahoo.com',
              'pierre.edgar@gmail.com'
              // 'nduval@dadeschools.net'
              // 'msteapot@me.com',
              // 'nduval@aol.com'
            ]
          },
          Source: 'EJP Foundation <admin@ejpfoundation.info>',
          Template: 'EJP_NEW_MENTEE',
          TemplateData: `
            { 
              \"NAME\":\"${name}\", 
              \"PHONE\":\"${phone}\",
              \"EMAIL\":\"${email}\",
              \"PREFERRED_CONTACT_METHOD\":\"${preferredContactMethod}\",
              \"INTEREST_AND_HOBBIES\":\"${interest_and_hobbies}\",
              \"WHY_PARTICIPATE\":\"${why_participate}\",
              \"DESCRIBE_SCHOOL_PERFORMANCE\":\"${describe_school_performance}\",
              \"HAS_FRIENDS\":\"${has_friends}\",
              \"CHILD_HAVING_PROBLEMS\":\"${child_having_problems}\",
              \"ADDITIONAL_BACKGROUND_INFO\":\"${additional_background_info}\",
              \"ANY_RELIGIOUS_PREFERENCES\":\"${any_religious_preferences}\"
            }
          `,
          ReplyToAddresses: [
            'thadduval.lavud@gmail.com',
            'faisalhuda37@yahoo.com',
            'pierre.edgar@gmail.com'
          ],
          ConfigurationSetName: 'Email_Delivery_Status',
          Tags: [
            {
              Name: "BatchId",
              Value: "EJP"
            }
          ]
        }

        const data = await sesClient.send(new SendTemplatedEmailCommand(params))
        
        res.status(200).json({ message: "You have successfully applied." })
      } catch(e) {
        res.status(500).json({ message: e.toString() })
        return
      }
      break
    default:
      res.setHeader('Allow', ['POST'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }

}
