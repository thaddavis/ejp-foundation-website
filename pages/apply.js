import Navbar from '../components/Navbar';
import Hero from '../components/Hero';
import Footer from '../components/Footer';

export default function Apply() {

    return (
        <>
            <Navbar />

            <Hero
                title={"Apply"}
                subtitle={"Apply as a mentee or mentor."}
            />

            <section className="articles">

                <div className="article__content container container--pall">

                    <div className="article__grid">

                        <a href="/mentee-app" className="article__item">
                            <div className="article__text">
                                <div className="article__author"></div>
                                <div className="article__title">Mentees</div>
                                <div className="article__description">Click here to apply as a Mentee</div>
                            </div>
                        </a>

                        <a href="/mentor-app" className="article__item">
                            <div className="article__text">
                                <div className="article__author"></div>
                                <div className="article__title">Mentors</div>
                                <div className="article__description">Click here to apply as a Mentor</div>
                            </div>
                        </a>

                    </div>
                </div>

            </section>

            <Footer/>
        </>
    )

}

