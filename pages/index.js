import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

import Navbar from '../components/Navbar';
import Hero from '../components/Hero';
import Footer from '../components/Footer';

export default function Home() {
  return (
    <div>
      <Head>
        <title>EJP Foundation</title>
        <meta name="description" content="EJP Foundation" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Navbar/>

      <Hero 
        title={"Edgard J. Pierre Foundation"}
        subtitle={"A non-profit organization to support and advance the lives of exceptional youth in Little Haiti."}
      />

      <section className="articles">

        <div className="article__content container container--pall">

          <div className="article__grid">

            <a href="/about" className="article__item">
              <div className="article__image" style={{
                backgroundImage: "url('/images/haiti-flag.jpg')"
              }}></div>
              <div className="article__text">
                <div className="article__author"></div>
                <div className="article__title">About</div>
                <div className="article__description">Read our Mission Statement</div>
              </div>
            </a>

            <a href="/apply" className="article__item">
              <div className="article__image" style={{
                backgroundImage: "url('/images/little-haiti.jpg')"
              }}></div>
              <div className="article__text">
                <div className="article__author"></div>
                <div className="article__title">Apply</div>
                <div className="article__description">Apply to be a Mentor or Mentee online</div>
              </div>
            </a>

          </div>
        </div>
      </section>

      {/* <section className="feature">

        <div className="feature__content container container--pall">

          <div className="feature__intro">
            <h2>Why choose Easybank?</h2>
            <p>
              We leverage Open Banking to turn your bank account into your financial hub. Control
              your finances like never before.
            </p>
          </div>

          <div className="feature__grid">

            <div className="feature__item">
              <div className="feature__icon"><img src="/images/icon-online.svg" /></div>
              <div className="feature__title">
                Online Banking
            </div>
              <div className="feature__description">
                Our modern web and mobile applications allow you to keep track of your finances
                wherever you are in the world.
            </div>
            </div>

            <div className="feature__item">
              <div className="feature__icon"><img src="/images/icon-budgeting.svg" /></div>
              <div className="feature__title">
                Simple Budgeting
            </div>
              <div className="feature__description">
                See exactly where your money goes each month. Receive notifications when you’re close to hitting your limits.
            </div>
            </div>

            <div className="feature__item">
              <div className="feature__icon"><img src="/images/icon-onboarding.svg" /></div>
              <div className="feature__title">
                Fast Onboarding
            </div>
              <div className="feature__description">
                We don’t do branches. Open your account in minutes online and start taking control of your finances right away.
            </div>
            </div>

            <div className="feature__item">
              <div className="feature__icon"><img src="/images/icon-api.svg" /></div>
              <div className="feature__title">
                Open API
            </div>
              <div className="feature__description">
                Manage your savings, investments, pension, and much more from one account. Tracking your money has never been easier.
            </div>
            </div>

          </div>
        </div>

      </section> */}

      <Footer/>

    </div>
  )
}
