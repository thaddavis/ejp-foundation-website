import Navbar from '../components/Navbar';
import Hero from '../components/Hero';
import MissionStatement from '../components/MissionStatement';
import Footer from '../components/Footer';

export default function About() {

    return (
        <>
            
            <Navbar />

            <Hero
                title={"Mission"}
                subtitle={"Scroll up and read about our mission."}
            />

            <MissionStatement/>
             
            <Footer/>
        </>
    )

}