import React from 'react';
import Link from 'next/link'

const MissionStatement = () => {
    return (
        <>
            <div className="about" style={{
                padding: "1em 2em"
            }}>
                <div>
                    <p>
                        <span></span>
                    </p>
                </div>
                <div >
                    <h2>Edgard J. Pierre Foundation</h2>
                </div>
                <p>
                    <b>Goals:</b>&nbsp;
                    This mentoring program will empower and prepare mentees by providing them with character value identification, personal assessments, academic goals and skills, career exploration and planning, and job readiness skills.
                    It will embrace this stance and work with others in the community, together advocating for meaningful and systemic social change. Mentoring can and should translate to larger community improvements as part of a more expansive movement toward increased equity and justice in our society.
                </p>
                <p>
                    <b>Project:</b>&nbsp;
                    The curriculum incorporates character development programs that are proven to help youth gain a better understanding of their values. Our program will be implemented into schools and other non-profit agencies throughout the country as a prevention and intervention program through mentoring, workshops and classroom sessions. This mentoring program will empower and prepare mentees by providing them with character value identification, personal assessments, academic goals and skills, career exploration and planning, and job readiness skills.
                </p>
                <p>
                    <b>Objectives:</b>&nbsp;
                    Specific, measurable, attainable, relevant and time-bound. These objectives are meant to provide direction to program participants, establish key performance indicators and help organizational leaders understand the reason for their support. The structure provides participants a mentoring workflow to follow and is critical to help participants achieve productive learning that reaches defined goals. Similarly, flexibility is essential to support varying individual mentoring needs across specific learning goals, preferences, and learning styles. The program is targeted to help young Haitians between the ages of 12-18 years of age.
                </p>
                <p>
                    <b>Location:</b>&nbsp;
                    212 NE 59 Terrace Miami Florida 33137 (aka Little Haiti Cultural Center)
                </p>
                <p>
                    <small>
                        The LHCC will be used for group gatherings, lectures, movies and guest lectures and historical events once a month from 6-8 pm, on preferably a Monday or Wednesday evening.
                    </small>
                </p>
                <p>
                    <b>Enrollment:</b>&nbsp;
                    Only an application is needed along with parental consent.
                </p>
                <p>
                    <b>Mentoring style:</b>&nbsp;
                    1:1 relationship, depending on the mentees goal for the future. Group mentoring is also available. Time invested between mentor and mentees: typically weeks or months. Monthly group meetings and activities. 1:1 activity weekly or bi-monthly.
                </p>
                <h2>
                    Mentor and Mentees:
                </h2>
                <p>
                    A productive mentoring relationship depends on a good fit to ensure that all youth have the support they need through mentoring relationships to succeed at home, school, and ultimately, life. Data shows that youth of color, regardless of socio-economic background, are disproportionately at risk throughout their life. For example, large disparities still remain in reading proficiency. Additionally, the disproportionate number of Blacks and Hispanics who are unemployed or involved in the criminal justice system alone is a perilous drag on state budgets, and undermines family and community stability.
                </p>
                <p>
                    Participants will bring various competencies, backgrounds, learning styles and needs. A great match for one person may not be a good match for another. The program will evaluate various mentor-mentee match combinations before finalizing pairs in order to maximize the quality of the match and will give mentees a say in the matching process by allowing them to select a particular mentor or submit their top choices.
                </p>
                <p>
                    As mentors enter into the mentoring relationship, they need to develop an awareness of critical consciousness. Developing critical consciousness requires an understanding of the unique challenges each child faces. The development of this consciousness requires an examination of various cultural issues. This is facilitated through an understanding and development of cultural competence or cultural humility. The term cultural competence has been used to describe an individuals competency in understanding ones own biases. The term cultural humility highlights the notion that one is never done when it comes to cultural understanding. One does not reach a level of competence and become an expert. Cultural humility supports the notion that we should always be listening, learning, and reflecting. Finally, as mentioned previously, critical mentoring champions a strengths-based approach to mentoring. Each mentee has specific strengths just as each culture and environment in which the mentee lives has particular strengths. Recognizing, drawing on and developing these strengths is a key component of the critical mentoring approach.
                </p>
                <p>
                    <i>Strive for mutual benefits.</i>&nbsp;
                    The relationship should be defined from the beginning as mutually beneficial. Each participant has committed to the relationship by choice. Each should openly share his or her goals for the relationship and work collaboratively to help achieve them.
                </p>
                <p>
                    <i>Commit to honesty.</i>&nbsp;
                    The participants should be willing to candidly share what they expect to gain from the relationship and their vision for getting there. They should be prepared to offer frank feedback as appropriate, even if the feedback is critical.
                </p>
                <p>
                    <i>Listen and learn.</i>&nbsp;
                    Mutual benefit and honesty can only be achieved when both members feel their viewpoints are heard and respected. Mentors, especially, need to remember that the relationship is not primarily about them. Co-mentors should not be intimidated or made to feel their views are not valued.
                </p>
                <p>
                    <i>Build a working partnership.</i>&nbsp;
                    Consider structuring a working partnership that includes project consultation or active collaborations rooted in the common ground of shared professional goals. These collaborations can lead to discoveries about each participant&#39;s preferred working style, daily obligations, and professional aspirations.
                </p>
                <p>
                    <i>Lead by example.</i>&nbsp;
                    Actions create the most lasting impression.
                </p>
                <p>
                    <i>Be flexible.</i>&nbsp;
                    It might help a mentoring relationship to have defined goals, but the process may be as important or more so than the goals.
                </p>
                <h2>
                    Potential Mentor participants:
                </h2>
                <p><span>Edgar Pierre MD</span></p>
                <p><span>Adam Gordon MD</span></p>
                <p><span>William Burns MD</span></p>
                <p><span>Faisal Huda MD</span></p>
                <p><span>Edouard Duval-Carrie</span></p>
                <p><span>Thadeus Duval</span></p>
                <p><span>Richard Brown</span></p>
                <p><span>Henson Destine</span></p>
                <p><span>Carl-Phillipe Juste</span></p>
                <p><span>Ciara Sinon MD</span></p>
                <p><span>Monique Phillipeaux</span></p>
                <p><span>Daphne Pierre</span></p>
                <h2>Board of Directors:</h2>
                <p><span>Edgar Pierre MD</span></p>
                <p><span>Adam Gordon MD</span></p>
            </div>
        </>
    );
};

export default MissionStatement;