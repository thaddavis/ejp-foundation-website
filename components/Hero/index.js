const Hero = (props) => {

    const {
        title,
        subtitle
    } = props

    return (
        <section className="hero">
        <div className="container">
          {/* <div className="hero__image"></div> */}

          <div className="hero__text container--pall">
            <h1>
              {title}
            </h1>
            <p>
              {subtitle}
            </p>
          </div>
        </div>
      </section>
    )

}

export default Hero;