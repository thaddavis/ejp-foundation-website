import React, { useEffect, useState } from 'react'
import cn from 'classnames'
import Link from 'next/link'

const Footer = () => {

    return (
        <>
            <footer className="footer">
                <div className="container">

                    <div className="footer__copyright">
                        &copy; EJP Foundation. All Rights Reserved.
                    </div>

                </div>
            </footer>
        </>
    );
};

export default Footer;