import React, { useEffect, useState } from 'react'
import cn from 'classnames'
import Link from 'next/link'

const Navbar = () => {

    return (
        <>
            <header className="header">
                
                <div className="overlay has-fade"></div>

                <nav className="container container--pall flex flex-jc-sb flex-ai-c">
                    <a href="/" style={{
                        color: "#9698a6",
                        textDecoration: "none"
                    }}>

                        <span className="header__logo">
                            EJP
                        </span>

                    </a>

                    <a onClick={
                        () => {
                            
                            const body = document.querySelector('body');
                            const header = document.querySelector('.header');
                            const fadeElems = document.querySelectorAll('.has-fade');

                            if (header.classList.contains('open')) { // Close Hamburger Menu
                                
                                body.classList.remove('noscroll');
                                header.classList.remove('open');    
                                fadeElems.forEach(function(element){
                                    element.classList.remove('fade-in');
                                    element.classList.add('fade-out');
                                });
                                
                            } else { // Open Hamburger Menu
                                
                                body.classList.add('noscroll');
                                header.classList.add('open');
                                fadeElems.forEach(function(element){
                                    element.classList.remove('fade-out');
                                    element.classList.add('fade-in');
                                });

                            }
                            
                        }
                    } id="btnHamburger" href="#" className="header__toggle hide-for-desktop">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>

                    <div className="header__links hide-for-mobile">
                        <a href="/about">About</a>
                        <a href="/apply">Apply</a>
                    </div>
                </nav>

                <div className={`
                    header__menu
                    has-fade
                `}>
                    <a href="/about">About</a>
                    <a href="/apply">Apply</a>
                </div>

            </header>
        </>
    );
};

export default Navbar;